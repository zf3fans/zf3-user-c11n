<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Console;

use Zf3Lib\UserC11n\Service\Telegram\Bot\FxLevelsNotificatorBot;
use Zf3Lib\UserC11n\Service\Telegram\BotManager;
use Zf3Lib\Lib\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UcTelegramBotSetup extends AbstractCommand
{
    protected static $defaultName = 'uc:telegram-bot-setup';

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
    }

    private BotManager $botManager;
    public function init(BotManager $botManager): UcTelegramBotSetup
    {
        $this->botManager = $botManager;
        return $this;
    }

    public function botSetup(): int
    {
        $bot = $this->botManager->getBotBySlug(FxLevelsNotificatorBot::BOT_SLUG);
        $response = $bot->reqWebHooks(true);

        return $response->isOk()
            ? self::SUCCESS
            : self::FAILURE;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        set_time_limit(15);
        return $this->botSetup();
    }
}