<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Console;

use Zf3Lib\Lib\Helper\System;
use Zf3Lib\UserC11n\Service\Notification;
use Zf3Lib\Lib\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UcDeliver extends AbstractCommand
{
    protected static $defaultName = 'uc:deliver';

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
    }

    private Notification\Manager $notificationManager;
    public function init(Notification\Manager $notificationManager): UcDeliver
    {
        $this->notificationManager = $notificationManager;
        return $this;
    }

    public function deliver(): int
    {
        $this->notificationManager->doTasks($this->notificationManager->getListForDeliver());
        return self::SUCCESS;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);

        if (System::isProcessLocked('uc:deliver')) {
            return self::SUCCESS;
        }

        ini_set('memory_limit', '4G');
        set_time_limit(0);

        return $this->deliver();
    }
}