<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Controller;

use Laminas\Http;
use Laminas\View\Model\ViewModel;
use Zf3Lib\Lib\Controller\Controller as AbstractController;
use Zf3Lib\UserC11n\Service\Telegram;
use JsonException;
use Exception;


class TelegramController extends AbstractController
{
    private function getMessage(): ?array
    {
        $content = file_get_contents("php://input");
        if ($content === false) {
            return null;
        }

        try {
            $update = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            return null;
        }

        return $update['message'] ?? null;
    }

    /**
     * Обработка сообщений ботом
     */
    public function webHooksAction(): ViewModel
    {
        $message = $this->getMessage();
        if ($message === null) {
            return $this->error(
                Http\Response::STATUS_CODE_400,
                true,
            );
        }

        /** @var Telegram\BotManager $botManager */
        $botManager = $this->getService(Telegram\BotManager::class);

        $botSlug = $this->params('bot_slug');
        try {
            $bot = $botManager->getBotBySlug($botSlug);
            $bot->processMessage($message);
        } catch (Exception $e) {
            log2file($e->getMessage());
        }

        return (new ViewModel())
            ->setTemplate('view/blank')
            ->setVariable('content', '');
    }
}