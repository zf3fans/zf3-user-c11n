<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n;

use Zf3Lib\Lib\Db;
use Laminas\ServiceManager\ServiceManager;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class Module
 * @package UserC11n - User Communications
 */
class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getControllerConfig(): array
    {
        return [
            'factories' => [
                Controller\TelegramController::class => fn (ServiceManager $sm) => new Controller\TelegramController($sm),
            ],
        ];
    }

    #[ArrayShape(['factories' => "\Closure[]"])]
    public function getServiceConfig(): array
    {
        return [
            'factories' => [
                Service\Notification\Manager::class =>
                    fn (ServiceManager $sm) =>
                        new Service\Notification\Manager(
                            $sm->get(Service\Notification\EngineFactory::class),
                            $sm->get(DbGateway\Notification\Tasks::class),
                            $sm->get(\Laminas\View\Renderer\PhpRenderer::class),
                        ),

                Service\Notification\EngineFactory::class =>
                    fn (ServiceManager $sm) =>
                        new Service\Notification\EngineFactory($sm),
                Service\Telegram\BotManager::class =>
                    fn (ServiceManager $sm) =>
                        new Service\Telegram\BotManager($sm),

                DbGateway\Notification\Tasks::class =>
                    fn (ServiceManager $sm) =>
                        new DbGateway\Notification\Tasks(
                            $sm->get(Db\AdapterManager::class),
                        ),

                Console\UcDeliver::class =>
                    fn (ServiceManager $sm) =>
                        (new Console\UcDeliver())->init(
                            $sm->get(Service\Notification\Manager::class),
                        ),
                Console\UcTelegramBotSetup::class =>
                    fn (ServiceManager $sm) =>
                        (new Console\UcTelegramBotSetup())->init(
                            $sm->get(Service\Telegram\BotManager::class),
                        ),
            ],
        ];
    }
}