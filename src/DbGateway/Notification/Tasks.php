<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\DbGateway\Notification;

use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;
use Zf3Lib\Lib\Helper\Arr;
use JetBrains\PhpStorm\ArrayShape;
use Laminas\Db\Sql;

class Tasks extends DbModel
{
    public const TABLE = 'uc_tasks';

    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'task_id',
                'channel',
                'template',
                'receiver',
                'subject',
                'text',
                'status',
                'created_at',
                'updated_at',
            ],
        ];
    }

    public function primaryKey(): string
    {
        return 'task_id';
    }

    public function insert(array $record = []): int
    {
        $record['created_at'] = $record['updated_at'] = date('Y-m-d H:i:s');
        return parent::insert($record);
    }

    public function update($data, $where = null): int
    {
        $data['updated_at'] = date('Y-m-d H:i:s');
        return parent::update($data, $where);
    }

    public function getListByParams(array $params): array
    {
        return $this->findList(
            $this->_getWhereByParams($params),
            $this->_getOrderByParams($params),
            $this->_getLimitByParams($params)
        );
    }

    private function _getWhereByParams(array $params): Sql\Where
    {
        $where = new Sql\Where();

        foreach (['task_id', 'channel', 'template', 'receiver', 'status'] as $field) {
            if (isset($params[$field])) {

                $values = is_array($params[$field])
                    ? $params[$field]
                    : [ $params[$field] ];
                $values = Arr::filterArrayOfString($values);
                $where->addPredicate(new Sql\Predicate\In($field, $values));
            }
        }

        return $where;
    }

    private function _getOrderByParams(array $params)
    {
        return $params['order'] ?? null;
    }

    private function _getLimitByParams(array $params): ?int
    {
        return isset($params['limit'])
            ? (int) $params['limit']
            : null;
    }
}