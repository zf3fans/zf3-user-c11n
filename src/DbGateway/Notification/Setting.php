<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\DbGateway\Notification;

use Zf3Lib\Lib\Db\AbstractDbGateway as DbModel;
use JetBrains\PhpStorm\ArrayShape;

class Setting extends DbModel
{
    public const TABLE = 'notification_settings';
    
    #[ArrayShape(['default' => "string[]"])]
    protected function scenarios(): array
    {
        return [
            'default' => [
                'id',
                'channel',
                'receiver',
                'is_enabled',
                'updated_at',
                'created_at',
            ],
        ];
    }
}