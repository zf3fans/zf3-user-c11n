<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification;

use Laminas\ServiceManager\ServiceLocatorInterface;
use Zf3Lib\UserC11n\Service\Notification\Engine\AbstractEngine;
use Zf3Lib\UserC11n\Service\Notification\Exception;
use Laminas\ServiceManager\ServiceManager;

class EngineFactory
{
    private ServiceManager $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    const ENGINE_TYPE_EMAIL     = 'Email';
    const ENGINE_TYPE_SMS       = 'Sms';
    const ENGINE_TYPE_TELEGRAM  = 'Telegram';
    const ENGINE_TYPE_DEFAULT   = self::ENGINE_TYPE_EMAIL;

    const ENGINE_TYPES = [
        self::ENGINE_TYPE_EMAIL,
        self::ENGINE_TYPE_SMS,
        self::ENGINE_TYPE_TELEGRAM,
    ];

    protected static array $engines = [];

    /**
     * @param string $engineType
     * @return ?AbstractEngine
     */
    public function get($engineType = self::ENGINE_TYPE_DEFAULT): ?AbstractEngine
    {
        if (!in_array($engineType, self::ENGINE_TYPES)) {
            return null;
        }

        if (isset(self::$engines[$engineType])) {
            $engine = self::$engines[$engineType];
        } else {
            $engineTypeClass = __NAMESPACE__ . '\\Engine\\' . $engineType;
            /** @var AbstractEngine $engine */
            $engine = new $engineTypeClass($this->serviceManager);
            self::$engines[$engineType] = $engine;
        }
        return $engine;
    }
}