<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification\Engine;

use Zf3Lib\Lib\Helper;
use Zf3Lib\UserC11n\Service\Notification\Task;
use GuzzleHttp\Client as HttpClient;
use Psr\Http\Message\ResponseInterface as HttpResponse;

/**
 * TODO: Проверить, что это вообще работает. Когда-то видимо работало.
 * И если всё-таки работает, вынести код из _doTask в отдельный sms-провайдер, а то неsolidно как-то.
 */
class Sms extends AbstractEngine
{
    private array $config;
    
    protected function init(): void
    {
        parent::init();
        $this->config = Helper\Arr::get($this->serviceManager->get('config'), 'sms', []);
    }
    
    public function _doTask(): bool
    {
        $params = [
            'login'    => Helper\Arr::get($this->config, 'login'),
            'psw'      => Helper\Arr::get($this->config, 'password'),
            'phones'   => $this->task->receiver(),
            'mes'      => trim($this->task->text()),
        ];
        $url = 'https://smsc.ru/sys/send.php?' . http_build_query($params);
        
        $result = false;
        try {
            $httpClient = new HttpClient();
            /** @var HttpResponse $response */
            $response = $httpClient->request('GET', $url, [
                'verify'            => false,
                'timeout'           => 60,
                'connect_timeout'   => 5,
                'debug'             => true,
            ]);
            $result = true;
        } catch (\Exception $exception) {}
        return $result;
    }
}