<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification\Engine;

use Laminas\Json\Json;
use Zf3Lib\UserC11n\Service\Notification\Task;
use Zf3Lib\UserC11n\Service\Telegram\BotManager;
use Zf3Lib\UserC11n\Service\Telegram\Bot\{
    AbstractBot,
    FxLevelsNotificatorBot,
};

class Telegram extends AbstractEngine
{
    private FxLevelsNotificatorBot $bot;
    
    protected function init(): void
    {
        parent::init();
        
        /** @var BotManager $botManager */
        $botManager = $this->serviceManager->get(BotManager::class);
        $this->bot = $botManager->getBotBySlug(FxLevelsNotificatorBot::BOT_SLUG);
    }

    private function send(): bool
    {
        $messageData = Json::decode($this->task->text(), Json::TYPE_ARRAY);
        $method = $messageData['method'] ?? '';

        switch ($method) {
            case AbstractBot::METHOD_SEND_MESSAGE:
                $apiResponse = $this->bot->reqSendMessage(
                    $this->task->receiver(),
                    $messageData['text'] ?? '',
                    $messageData['parse_mode'] ?? AbstractBot::PARSE_MODE_MARKDOWN_V2,
                    (bool) ($messageData['protect_content'] ?? false),
                    (int) ($messageData['reply_to_message_id'] ?? 0),
                );
                break;

            case AbstractBot::METHOD_SEND_PHOTO:
                $apiResponse = $this->bot->reqSendPhoto(
                    $this->task->receiver(),
                    $messageData['photo'] ?? '',
                    $messageData['caption'] ?? '',
                    (bool) ($messageData['protect_content'] ?? false),
                );
                break;

            default:
                $this->setStatus(Task::STATUS_FAILED);
                return false;
        }

        $this->setStatus(
            ($apiResponse->isOk())
                ? Task::STATUS_DONE
                : Task::STATUS_FAILED
        );
        return $apiResponse->isOk();
    }

    public function _doTask(): bool
    {
        $this->setStatus(Task::STATUS_IN_PROGRESS);

        $result = $this->send();
        $this->setStatus(
            $result
                ? Task::STATUS_DONE
                : Task::STATUS_FAILED
        );

        return $result;
    }
}