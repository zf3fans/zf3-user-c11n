<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification\Engine;

use Laminas\ServiceManager\ServiceManager;
use Zf3Lib\UserC11n\Service\Notification\Task;

abstract class AbstractEngine
{
    protected ServiceManager $serviceManager;

    protected Task $task;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        $this->init();
    }

    protected function init(): void
    {
    }
    
    protected function getType(): string
    {
        $type = preg_replace('/^.+\\\\/', '', static::class);
        $type = strtolower($type);
        
        return $type;
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function doTask(Task $task): bool
    {
        $this->task = $task;
        return $this->_doTask();
    }

    abstract protected function _doTask(): bool;
    
    protected function setStatus(string $status): void
    {
        $this->task->setStatus($status);
    }
}