<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification\Engine;

use Zf3Lib\Lib\Helper;
use Zf3Lib\UserC11n\Service\Notification\Task;
use PHPMailer\PHPMailer;
use Exception;


class Email extends AbstractEngine
{
    private PHPMailer\PHPMailer $mailer;
    private array $config;

    protected function init(): void
    {
        parent::init();
        $this->config = Helper\Arr::get($this->serviceManager->get('config'), 'mailer', []);
        $this->initMailer();
    }
    
    private function initMailer(): void
    {
        $this->mailer = new PHPMailer\PHPMailer(true);
    
        $this->mailer->isSMTP();
        $this->mailer->Host = $this->config['host'];
        $this->mailer->SMTPAuth = true;
        $this->mailer->Username = $this->config['email'];
        $this->mailer->Password = $this->config['password'];
        $this->mailer->SMTPSecure = 'tls';
        $this->mailer->Port = $this->config['port'];
        $this->mailer->CharSet = 'UTF-8';
    
        try {
            $this->mailer->setFrom($this->config['email'], $this->config['name']);
        } catch (PHPMailer\Exception|Exception $e) {}
    }
    
    private function send(): bool
    {
        try {
            $this->mailer->addAddress($this->task->receiver());
        } catch (PHPMailer\Exception|Exception $e) {
            return false;
        }
        $this->mailer->Subject = $this->task->subject();
        $this->mailer->isHTML(true);
        $this->mailer->SMTPDebug = false;//true;
    
        $text = $this->task->text();
        $this->mailer->Body    = $text;
        $this->mailer->AltBody = $text;

        try {
            $result = $this->mailer->send();
        } catch (PHPMailer\Exception|Exception $e) {
            return false;
        }
        return $result;
    }
    
    protected function _doTask(): bool
    {
        $this->setStatus(Task::STATUS_IN_PROGRESS);

        $result = $this->send();
        $this->setStatus(
            $result
                ? Task::STATUS_DONE
                : Task::STATUS_FAILED
        );

        return $result;
    }
}