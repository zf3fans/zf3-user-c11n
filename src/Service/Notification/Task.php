<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification;

use DateTimeImmutable;
use InvalidArgumentException;
use Zf3Lib\Lib\Helper;

class Task
{
    public const STATUS_NEW            = 'new';
    public const STATUS_IN_PROGRESS    = 'in_progress';
    public const STATUS_DONE           = 'done';
    public const STATUS_FAILED         = 'failed';

    public const STATUSES = [
        self::STATUS_NEW,
        self::STATUS_IN_PROGRESS,
        self::STATUS_DONE,
        self::STATUS_FAILED,
    ];
    
    public function __construct(string $channel)
    {
        $this->id         = 0;
        $this->status     = self::STATUS_NEW;
        $this->receiver   = '';
        $this->subject    = '';
        $this->text       = '';
        $this->createdAt  = null;
        $this->updatedAt  = null;
        $this->setChannel($channel);
    }


    private int $id;

    public function id(): int
    {
        return $this->id;
    }

    public function setId(int $id): Task
    {
        $this->id = $id;
        return $this;
    }

    private string $subject;
    
    public function subject(): string
    {
        return $this->subject;
    }
    
    public function setSubject(string $subject): Task
    {
        $this->subject = $subject;
        return $this;
    }

    private string $text;
    
    public function text(): string
    {
        return $this->text;
    }
    
    public function setText(string $text): Task
    {
        $this->text = $text;
        return $this;
    }

    private string $channel;
    private string $engineType;
    
    public function channel(): string
    {
        return $this->channel;
    }

    public function getEngineType(): string
    {
        return $this->engineType;
    }

    public function setChannel(string $channel): Task
    {
        $this->channel    = mb_strtolower($channel);
        $this->engineType = ucfirst($channel);

        if (!in_array($this->engineType, EngineFactory::ENGINE_TYPES, true)) {
            throw new InvalidArgumentException("Unsupported engine type given: {$this->engineType}");
        }
        return $this;
    }

    private string $template;

    public function template(): string
    {
        return $this->template;
    }

    public function setTemplate(string $template): Task
    {
        $this->template = $template;
        return $this;
    }

    private string|int $receiver;
    
    public function receiver(): string|int
    {
        return $this->receiver;
    }

    public function setReceiver(string|int $receiver): Task
    {
        $this->receiver = $receiver;
        return $this;
    }


    private string $status;
    
    public function status(): string
    {
        return $this->status;
    }
    
    public function setStatus(string $status): Task
    {
        if (!in_array($status, self::STATUSES, true)) {
            return $this;
        }
        $this->status = $status;
        return $this;
    }

    private ?DateTimeImmutable $createdAt;

    public function createdAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $dateTime): Task
    {
        $this->createdAt  = Helper\DateTime::getDtiOrNull($dateTime ?: Helper\DateTime::DT_EMPTY);
        return $this;
    }

    private ?DateTimeImmutable $updatedAt;

    public function updatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(string $dateTime): Task
    {
        $this->updatedAt  = Helper\DateTime::getDtiOrNull($dateTime ?: Helper\DateTime::DT_EMPTY);
        return $this;
    }
}