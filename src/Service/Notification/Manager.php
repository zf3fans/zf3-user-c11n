<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Notification;

use Zf3Lib\Lib\Helper\Arr;
use Zf3Lib\UserC11n\DbGateway;
use Zf3Lib\UserC11n\Service\Notification\Engine\AbstractEngine;
use Laminas\View\{
    Model\ViewModel,
    Renderer\PhpRenderer,
    Exception as ViewException
};

class Manager
{
    private EngineFactory $engineFactory;
    private DbGateway\Notification\Tasks $tasksModel;
    private PhpRenderer $viewRenderer;

    public function __construct(
        EngineFactory $engineFactory,
        DbGateway\Notification\Tasks $tasksModel,
        PhpRenderer $viewRenderer,
    )
    {
        $this->engineFactory    = $engineFactory;
        $this->tasksModel       = $tasksModel;
        $this->viewRenderer     = $viewRenderer;
    }

    protected static array $engines = [];

    /**
     * @param string $engineType
     * @return AbstractEngine|null
     */
    public function getEngine(string $engineType = EngineFactory::ENGINE_TYPE_DEFAULT): ?AbstractEngine
    {
        if (!isset(self::$engines[$engineType])) {
            self::$engines[$engineType] = $this->engineFactory->get($engineType);;
        }
        return self::$engines[$engineType];
    }

    /**
     * Создаёт задание на отправку
     * @param string $template
     * @param string $channel
     * @param string|int $receiver
     * @param array $data
     * @return Task
     */
    public function generateTask(
        string $template,
        string $channel,
        string|int $receiver,
        array $data
    ): Task
    {
        $task = new Task($channel);
        $messageParts = $this->getMessageParts($template, $task->channel(), $data);
        return $task
            ->setTemplate($template)
            ->setSubject($messageParts['subject'])
            ->setText($messageParts['text'])
            ->setReceiver($receiver);
    }

    public function generateTaskByData(array $taskData): Task
    {
        $receiver = $taskData['receiver'] ?? '';
        if (is_numeric($receiver)) {
            $receiver = (int) $receiver;
        }

        return (new Task($taskData['channel'] ?? ''))
            ->setId(Arr::iget($taskData, 'task_id'))
            ->setTemplate($taskData['template'] ?? '')
            ->setReceiver($receiver)
            ->setStatus($taskData['status'] ?? '')
            ->setSubject($taskData['subject'] ?? '')
            ->setText($taskData['text'] ?? '')
            ->setCreatedAt($taskData['created_at'] ?? null)
            ->setUpdatedAt($taskData['updated_at'] ?? null);
    }

    /**
     * @param string $template
     * @param string $channel
     * @param array $params
     * @return array
     */
    private function getMessageParts(string $template, string $channel, array $params): array
    {
        $messageParts = [
            'subject' => '',
            'text' => '',
        ];

        foreach(['subject', 'text'] as $key) {
            try {
                $view = new ViewModel();
                $view->setTerminal(true)
                    ->setTemplate($template . '/' . $channel . '/' . $key . '.phtml')
                    ->setVariables($params);
                $messageParts[$key] = $this->viewRenderer->render($view);
            } catch (ViewException\DomainException|ViewException\InvalidArgumentException|ViewException\RuntimeException) {}
        }

        return $messageParts;
    }

    public function scheduleTask(Task $task): int
    {
        $task->setStatus(Task::STATUS_NEW);
        return $this->saveTask($task);
    }

    private function saveTask(Task $task): int
    {
        $taskData = [
            'channel'   => $task->channel(),
            'template'  => $task->template(),
            'receiver'  => $task->receiver(),
            'subject'   => $task->subject(),
            'text'      => $task->text(),
            'status'    => $task->status(),
        ];

        if ($task->id() !== 0) {
            $this->tasksModel->update($taskData, $task->id());
            $taskId = $task->id();
        } else {
            $taskId = $this->tasksModel->insert($taskData);
        }
        return $taskId;
    }

    private const DELIVERY_BULK_LIMIT = 100;

    public function getListForDeliver(): array
    {
        // TODO: переделать поиск заданий для отправки
        return $this->getListByParams([
            'status' => Task::STATUS_NEW,
            'order' => 'created_at ASC',
            'limit' => self::DELIVERY_BULK_LIMIT,
        ]);
    }

    /**
     * @param array $params
     * @return Task|null
     */
    public function getByParams(array $params): ?Task
    {
        return Arr::first($this->getListByParams($params));
    }

    /**
     * @param array $params
     * @return array
     */
    public function getListByParams(array $params): array
    {
        $list = [];

        $tasksData = $this->tasksModel->getListByParams($params);
        foreach ($tasksData as $taskData) {
            $list[] = $this->generateTaskByData($taskData);
        }
        return $list;
    }

    /**
     * @param Task[] $tasks
     */
    public function doTasks(array $tasks): void
    {
        foreach ($tasks as $task) {
            $this->doTask($task);
        }
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function doTask(Task $task): bool
    {
        $engine = $this->getEngine($task->getEngineType());

        $done = $engine !== null && $engine->doTask($task);
        $task->setStatus(
            $done
                ? Task::STATUS_DONE
                : Task::STATUS_FAILED
        );

        $this->saveTask($task);

        return $done;
    }
}