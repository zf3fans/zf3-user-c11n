<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Telegram;

use Zf3Lib\UserC11n\Service\Telegram\{Bot\AbstractBot, Bot\FxLevelsNotificatorBot};
use Laminas\ServiceManager\ServiceManager;
use InvalidArgumentException;
use RuntimeException;
use Exception;

class BotManager
{
    private ServiceManager $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    private static array $bots = [];

    private const BOTS = [
        FxLevelsNotificatorBot::BOT_SLUG => FxLevelsNotificatorBot::class,
    ];

    public function getBotBySlug(string $botSlug): AbstractBot
    {
        if (isset(self::$bots[$botSlug])) {
            return self::$bots[$botSlug];
        }

        if (!array_key_exists($botSlug, self::BOTS)) {
            throw new InvalidArgumentException("Unsupported telegram bot slug: {$botSlug}");
        }

        $botClass = self::BOTS[$botSlug];

        try {
            $bot = new $botClass($this->serviceManager);
        } catch (Exception $e) {
            throw new RuntimeException("Couldn't create bot object {$botSlug}: {$e->getMessage()}");
        }
        self::$bots[$botSlug] = $bot;

        return $bot;
    }
}