<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Telegram;

class ApiResponse
{
    private bool $isOk;

    public function isOk(): bool
    {
        return $this->isOk;
    }

    private int $curlErrorCode;

    public function curlErrorCode(): int
    {
        return $this->curlErrorCode;
    }

    private string $curlError;

    public function curlError(): string
    {
        return $this->curlError;
    }

    public function __construct(
        bool $isOk,
        int $curlErrorCode = 0,
        string $curlError = '',
    ) {
        $this->isOk = $isOk;
        $this->curlErrorCode = $curlErrorCode;
        $this->curlError = $curlError;
    }
}