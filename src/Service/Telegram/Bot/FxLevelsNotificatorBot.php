<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Telegram\Bot;

class FxLevelsNotificatorBot extends AbstractBot
{
    public const BOT_SLUG = 'fx_levels_notificator_bot';

    public function processMessage(array $message): void
    {
        if (empty($message)) {
            return;
        }

        return;
        // TODO!
        // $this->serviceManager->get()
        // process incoming message
        $message_id = (int) $message['message_id'];
        $chat_id = (int) $message['chat']['id'];
        if (isset($message['text'])) {
            // incoming text message
            $text = $message['text'];
        
            if (str_starts_with($text, "/start")) {
                $this->reqSendMessage($chat_id, 'Hello',);
            } elseif ($text === "Hello" || $text === "Hi") {
                $this->apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'Nice to meet you'));
            } elseif (str_starts_with($text, "/stop")) {
                // stop now
            } else {
                $this->apiRequestWebhook("sendMessage", array('chat_id' => $chat_id, "reply_to_message_id" => $message_id, "text" => 'Cool'));
            }
        } else {
            $this->apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'I understand only text messages'));
        }
    }
}