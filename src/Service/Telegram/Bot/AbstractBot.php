<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n\Service\Telegram\Bot;

use CURLFile;
use Laminas\ServiceManager\ServiceManager;
use Zf3Lib\Lib\Helper;
use Zf3Lib\UserC11n\Service\Telegram\ApiResponse;

abstract class AbstractBot
{
    /**
     * MUST be replaced in child bot class
     */
    public const BOT_SLUG = '';

    public const METHOD_SEND_MESSAGE = 'sendMessage';
    public const METHOD_SEND_PHOTO   = 'sendPhoto';
    public const METHOD_SET_WEBHOOK  = 'setWebhook';


    protected ServiceManager $serviceManager;

    protected array $config;
    protected string $webHooksUrl;
    protected string $apiUrl;
    
    public function __construct(ServiceManager $serviceManager)
    {
        $this->init($serviceManager);
    }
    
    protected function init(ServiceManager $serviceManager): void
    {
        $this->serviceManager = $serviceManager;
        
        $telegramConfig = Helper\Arr::get($this->serviceManager->get('config'), 'telegram', []);
        $this->config   = Helper\Arr::get($telegramConfig, static::BOT_SLUG, []);
        
        $token = Helper\Arr::get($this->config, 'token', '');
    
        $this->apiUrl      = 'https://api.telegram.org/bot' . $token . '/';
        $this->webHooksUrl =
            'https://' . HTTP_HOST .
            $this->serviceManager->get('ViewHelperManager')->get('url')->__invoke(
                'telegram/web-hooks',
                ['bot_slug' => $this->config['url_slug']]
            );
    }
    
    public function apiRequestWebHook(string $method, array $parameters): bool
    {
        $parameters["method"] = $method;
        
        header("Content-Type: application/json");
        echo json_encode($parameters);
        return true;
    }

    public function apiRequest(string $method, array $parameters, bool $isPost = true): ApiResponse
    {
        $url = $this->apiUrl . $method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($isPost) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        }
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $result = ($result === false)
            ? new ApiResponse(false, curl_errno($ch), curl_error($ch))
            : new ApiResponse(true);
        curl_close($ch);

        return $result;
    }

    /**
     * Обработка сообщения от пользователя
     *
     * @param array $message
     */
    abstract public function processMessage(array $message): void;


    // region Methods

    // region sendMessage

    public const PARSE_MODE_HTML        = 'HTML';
    public const PARSE_MODE_MARKDOWN    = 'Markdown';
    public const PARSE_MODE_MARKDOWN_V2 = 'MarkdownV2';

    public function reqSendMessage(
        string|int      $chatId,
        string          $text,
        string          $parseMode = self::PARSE_MODE_MARKDOWN_V2,
        bool            $protectContent = false,
        int             $replyToMessageId = 0,
    ): ApiResponse
    {
        $parameters = [
            'chat_id'               => $chatId,
            'text'                  => $text,
            'parse_mode'            => $parseMode,
            'protectContent'        => $protectContent,
        ];
        if ($replyToMessageId > 0) {
            $parameters['reply_to_message_id'] = $replyToMessageId;
        }

        return $this->apiRequest(
            self::METHOD_SEND_MESSAGE,
            $parameters,
        );
    }

    // endregion sendMessage


    // region sendPhoto

    public function reqSendPhoto(
        string|int      $chatId,
        string|CURLFile $photo,
        string          $caption = '',
        bool            $protectContent = false,
    ): ApiResponse
    {
        // Если строка, но указан внутренний путь к файлу, а не url
        if (is_string($photo) && !parse_url($photo)) {
            $photoPath = PUBLIC_PATH;
            if (!str_starts_with($photo, '/')) {
                $photoPath .= '/';
            }
            $photoPath .= $photo;

            if (!file_exists($photoPath)) {
                return new ApiResponse(false);
            }

            $mimeType = mime_content_type($photoPath);
            if ($mimeType === false) {
                return new ApiResponse(false);
            }

            $photo = curl_file_create(
                $photoPath,
                $mimeType,
                $caption
            );
        }

        return $this->apiRequest(
            self::METHOD_SEND_PHOTO,
            [
                'chat_id'           => $chatId,
                'photo'             => $photo,
                'caption'           => $caption,
                'protectContent'    => $protectContent,
            ],
        );
    }

    // endregion sendPhoto


    /**
     * @param bool $set Установить или удалить установку
     */
    public function reqWebHooks(bool $set = true): ApiResponse
    {
        return $this->apiRequest(self::METHOD_SET_WEBHOOK, ['url' => $set ? $this->webHooksUrl : '']);
    }

    // endregion Methods
}