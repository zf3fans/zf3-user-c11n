<?php
declare(strict_types=1);
namespace Zf3Lib\UserC11n;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Zf3Lib\UserC11n\Controller;

return [
    'router' => [
        'routes' => [
            'telegram' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/telegram',
                ],
                'child_routes' => [
                    /** @see Controller\TelegramController::webHooksAction() */
                    'web-hooks' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/web-hooks/:bot_slug/',
                            'defaults' => [
                                'controller' => Controller\TelegramController::class,
                                'action'     => 'web-hooks',
                            ],
                            'constraints' => [
                                'bot_slug' => '[a-z_]+',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'user-c11n' => __DIR__ . '/../view',
        ],
    ],
    'console' => [
        'commands' => [
            Console\UcDeliver::class,
            Console\UcTelegramBotSetup::class,
        ],
    ],
];
