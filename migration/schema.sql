-- Notifications

drop table if exists notification_settings;
create table notification_settings
(
    `id`         int auto_increment
        primary key,
    `channel`    enum ('email', 'sms', 'telegram') default 'email' not null,
    `receiver`   varchar(200)                                      not null,
    `is_enabled` tinyint(1)                                        not null,
    `created_at` datetime                                          not null,
    `updated_at` datetime                                          not null
);

drop table if exists uc_tasks;
create table uc_tasks
(
    `task_id`    bigint auto_increment
        primary key,
    `channel`    enum ('email', 'sms', 'telegram')             not null,
    `template`   varchar(255) default ''                       not null,
    `receiver`   varchar(255)                                  not null,
    `subject`    varchar(255) default ''                       not null,
    `text`       text                                          null,
    `status`     enum ('new', 'in_progress', 'failed', 'done') not null,
    `created_at` datetime                                      not null,
    `updated_at` datetime                                      not null
);
create index uc_tasks_created_at_IDX on uc_tasks (`created_at`);
create index uc_tasks_receiver_IDX on uc_tasks (`receiver`);
create index uc_tasks_receiver_template_IDX on uc_tasks (`receiver`, `template`);